package main;

public class MaxProfit {
    public int calculateProfit(int[] stockPrice) {
        int maximumProfit = 0;
        int minValue = stockPrice[0];
        int difference;
        for (int i = 1; i < stockPrice.length; i++) {
            difference = stockPrice[i] - minValue;
            if (difference > maximumProfit) {
                maximumProfit = difference;
            }
            if (minValue > stockPrice[i]) {
                minValue = stockPrice[i];
            }
        }
        return maximumProfit;
    }
}
