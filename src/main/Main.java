package main;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        MaxProfit maxProfit = new MaxProfit();
        Scanner userInput = new Scanner(System.in);
        System.out.print("please enter number of days :");
        int dayNumber = Integer.parseInt(userInput.nextLine());
        while (dayNumber < 1 || dayNumber > 1000) {
            System.out.print("please enter proper number (between 1 and 1000) :");
            dayNumber = Integer.parseInt(userInput.nextLine());
        }
        int[] stockPrice = new int[dayNumber];
        for (int i = 0; i < dayNumber; i++) {
            System.out.print(String.format("please enter price of stock for day %d : ", i + 1));
            stockPrice[i] = Integer.parseInt(userInput.nextLine());
        }
        int maximumProfit = maxProfit.calculateProfit(stockPrice);
        System.out.println("your maximum profit is : " + maximumProfit);
    }
}
